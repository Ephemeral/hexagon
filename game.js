  function initgame() {
    minecount = 91;
    createboard();
    draw();

  }
  var timetimer = null;
  var seconds;

  function timer() {
    seconds = 0;
    timetimer = setInterval(function ()

      {
        ++seconds;
        var minutes = Math.floor(seconds / 60);
        var secs = seconds % 60;
        var display = "Time: " + minutes + ":" + secs;
        document.getElementById("timer").textContent = display;
      }, 1000);
  }

  function canceltimer() {
    clearInterval(timetimer);
  }

  function resettimer() {
    seconds = 0;
  }

  function createboard() {
    for (var row = 0; row < length; ++row) {
      cells[row] = [];
      for (var col = 0; col < width; ++col) {
        cells[row][col] = new Hexagon(col, row);
      }
    }
  }

  function draw() {
    let podwrap = document.getElementsByClassName("pod-wrap");

    for (var row = 0; row < length; ++row) {

      for (var col = 0; col < width; ++col) {
        if (!cells[row][col].isdisabled()) {
          var pod = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
          //<polygon stroke="#000000" stroke-width="0.5" points="5,-10 -5,-10 -10,0 -5,10 5,10 10,0" />
          pod.setAttribute("stroke", "#000000");
          pod.setAttribute("stroke-width", "0.5");
          pod.setAttribute("points", "5,-10 -5,-10 -10,0 -5,10 5,10 10,0");

          var translatex = col * hexagonwidth;

          var translatey = row * hexagonlength + offset;

          if (col % 2 != 0) {
            translatey += offset;
          }

          var translate = "translate(" + translatex + "," + translatey + ")";

          pod.setAttribute("transform", translate);

          pod.setAttribute("gridx", col);

          pod.setAttribute("gridy", row);

          pod.setAttribute("centerx", translatex);

          pod.setAttribute("centery", translatey);

          pod.addEventListener('click', function () {
            handleclick(this);
          });
          pod.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");

          cells[row][col].appendelement(pod);
          podwrap[0].appendChild(pod);

        }

      }
    }
  }

  function handleclick(element) {
    send({
      PacketId: 2,
      XPosition: parseInt(element.getAttribute("gridx")),
      YPosition: parseInt(element.getAttribute("gridy"))
    });
  }


  function reveal(cell) {
    var element = cell.getelements();
    element.forEach(e =>{
      if(e.getAttribute("centerx")!=null){
        element = e;
      }
    });
    if (!cell.isempty()) {
      
      element.style.fill = "transparent";
      let podwrap = document.getElementsByClassName("pod-wrap");

      let text = document.createElementNS("http://www.w3.org/2000/svg", "text");

      text.setAttribute("class", "small");

      text.setAttribute("x", element.getAttribute("centerx"));

      text.setAttribute("y", element.getAttribute("centery"));




      if (cell.getminecount() == 1) {
        text.setAttribute("id", "low");
      } else if (cell.getminecount() < 3) {
        text.setAttribute("id", "medium");
      } else if (cell.getminecount() >= 3) {
        text.setAttribute("id", "high");
      }


      cell.appendelement(text);
      text.textContent = cell.getminecount();
      podwrap[0].appendChild(text);
    } else {
      element.style.fill = "rgba(239, 189, 98, 0.27)";
    }
  }


  function destroy() {
    let podwrap = document.getElementsByClassName("pod-wrap");
    for (var row = 0; row < length; ++row) {
      for (var col = 0; col < width; ++col) {
        let elements = cells[row][col].getelements();
        elements.forEach(child => {
          podwrap[0].removeChild(child);
        });

      }
    }
  }

  function updateminecount() {
    --minecount;
    document.getElementById("minesleft").textContent = "Mines left: " + minecount;
  }

  function drawmine(cell,colour) {


    let podwrap = document.getElementsByClassName("pod-wrap");
    var element = cell.getelements();
    element.forEach(e =>{
      if(e.getAttribute("centerx")!=null){
        element = e;
      }
    });

    let mine = document.createElementNS("http://www.w3.org/2000/svg", "g");
    let x = parseInt(element.getAttribute("centerx"))-5;
    let y = parseInt(element.getAttribute("centery"))+5;
    mine.setAttribute("transform","translate("+x+","+y+") scale(0.005000,-0.005000)");
    mine.setAttribute("fill",colour);
    mine.setAttribute("stroke","none");
    let path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    path.setAttribute("d","M773 2225 c-29 -8 -71 -24 -93 -35 l-40 -20 0 -1005 0 -1005 40 0 40 0 0 578 0 579 38 12 c56 19 229 25 307 12 37 -7 129 -35 203 -62 74 -28 169 -57 211 -66 135 -28 320 -7 420 46 21 12 21 15 21 456 0 245 -2 445 -5 445 -3 0 -23 -9 -43 -19 -77 -40 -159 -54 -282 -48 -122 5 -108 1 -390 103 -99 35 -113 38 -240 41 -97 2 -150 -1 -187 -12z");
    mine.appendChild(path);
    cell.appendelement(mine);
    podwrap[0].appendChild(mine);
  }

  /*
  Begin working on game
  */