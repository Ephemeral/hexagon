var ws;
var MyPlayerId;
var timerId = 0;

function Connect() {
  //
  if ("WebSocket" in window) {

    initgame();
    ws = new WebSocket("wss://cynosure.pw:8080");

    ws.onopen = function () {


      //reset the game with game.init();
      //game.init();

      function keepAlive() {
        var timeout = 20000;
        if (ws.readyState == ws.OPEN) {
          send({PacketId: 21});
        }
        timerId = setTimeout(keepAlive, timeout);
      }
      keepAlive();
    };
    //RecordingStream test! 
    ws.onmessage = function (evt) {
      var received_msg = evt.data;
      HandlePacketId(received_msg);
    };

    ws.onclose = function () {
      Live = false;

      alert("Server offline try again soon!");
    };
    function cancelKeepAlive() {  
      if (timerId) {  
          clearTimeout(timerId);  
          timerId = 0;
      } 
      
  }
  cancelKeepAlive();
  
  } else {

    // The browser doesn't support WebSocket

  }
}

function send(packet) {
  ws.send(JSON.stringify(packet));
}

function HandlePacketId(received_msg) {
  var Packet = JSON.parse(received_msg);
  switch (Packet.PacketId) {

    case PacketId.Move:
      {
        HandleMovePacket(Packet);
      }
      break;
    case PacketId.MyId:
      {

        MyPlayerId = Packet.Id;
        // document.getElementById("myid").textContent = "My Id: "+MyPlayerId;
        // game.setSelfId(MyPlayerId);
      }
      break;
    case PacketId.InGame:
      {

        alert("Game begun!");
        
      }
      break;
      case PacketId.GameOver:{

      }
    case PacketId.Win:
      {
        document.getElementById("result").textContent = "Result: Winner";
        alert("You win!");
      }
    case PacketId.Lose:
      {
        document.getElementById("result").textContent = "Result: Loser";
        alert("You lose!");
      }
      break;
    case PacketId.Turn:
      {
        // game.setTurn("Mine");
        document.getElementById("turn").textContent = "Turn: Mine";
        resettimer();
      }
      break;
    case PacketId.ApponentTurn:
      {
        document.getElementById("turn").textContent = "Turn: Apponent";
        resettimer();
        //game.setTurn("Opponent");
      }
      break;
      case PacketId.Pong:{

      }
      break;
    default:
      console.log("Packet id not found");
      break;
  }
}

function HandleMovePacket(Packet) {
  console.log(Packet);

  Packet.Position.forEach(element => {

    let cell = cells[element.YPosition][element.XPosition];

    cell.reveal(element.Cell); 
    reveal(cell);
    if(Packet.PlayerScored){
      if(Packet.Id == MyPlayerId){
        document.getElementById("myscore").textContent = "My score: "+Packet.TotalScore;
        cell.setmine();
        drawmine(cell,"#0000ff");
      }
      else{
        document.getElementById("apponent").textContent = "Apponent: "+Packet.TotalScore;
        cell.setmine();
        drawmine(cell,"#ff0000");
      }
      updateminecount();

    }

  });


}


var PacketId = {
  Win: 0,
  Lose: 1,
  Move: 2,
  Turn: 3,
  ApponentTurn: 4,
  InGame: 5,
  Rejoin: 6,
  Time: 7,
  Tie: 8,
  MyId: 9,
  Nop0: 10,
  Score: 11,
  Status: 12,
  GameOver: 13,
  LeaveGame: 14,
  Nop1: 15,
  ChallengeRequest: 16,
  ChallengeResponse: 17,
  ToggleRequests: 18,
  AllPlayers: 19,
  Nop2: 20,
  Ping: 21,
  Pong: 22
};