var Hexagon = function(x,y){
    this.elements = [];
    this.x = x;
    this.y = y;
    this.mine = false;
    this.revealed = false;
    this.minecount = 0;
    this.empty = false;
    this.isdisabled = false;


    this.appendelement = function(element){
        this.elements.push(element);
    }
    this.getelements = function(){
        return this.elements;
    }
    
    this.setmine = function(){
        this.mine = true;
        this.minecount = -1;
    }
    this.ismine = function(){
        return this.mine;
    }
    this.setminecount = function(minecount){
        this.minecount = minecount;
        if(minecount == 0){
            this.empty = true;
        }
    }
    this.getminecount = function(){
        return this.minecount;
    }
    this.setrevealed = function(){
        // this.revealed = true;
    }
    this.reveal = function(minecount){
        // setrevealed();
        this.revealed = true;
        this.setminecount(minecount);
    }
    this.disable = function(){
        this.disabled = true;
    }
    this.isrevealed = function(){
        return this.revealed;
    }
    this.isempty = function(){
        return this.empty;
    }
    this.getx = function(){
        return this.x;
    }
    this.gety = function(){
        return this.y;
    }
    this.isdisabled = function(){
        return this.disabled;
    }
}
